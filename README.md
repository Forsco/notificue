# notificue
A Notification System replacement for Windows 10 (64-bit only).

I got tired of the default notifications not really working well, so I made this.

![a notification](https://i.imgur.com/8UaSxZu.png)

## Generic instructions

Left-click the tray icon to bring up the log, or right-click it to quit: ![tray icon](https://i.imgur.com/uLSZQzM.png)

You can customize the appearance of the notifications in the config `notificue.ini`. Should be self-explanatory.

If notifications don't show up for you, please create an issue.

PR's allowed if you follow the codestyle.

